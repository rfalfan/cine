package io.dflabs.dflabscinema.model.objects

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/**
 * Created by danielgarciaalvarado on 4/28/18.
 * DflabsCinema
 */
@Entity(tableName = "movie")
data class Movie(
        @PrimaryKey
        var id: Long? = -1,

        /*
            If we need to change the column name in the database we use:
            @ColumnInfo(name = "name")
         */

        var title: String,

        @SerializedName("thumbnail")
        var image: String?,

        @SerializedName("is_featured")
        var isFeatured: Boolean = false,

        var classification: String?
)