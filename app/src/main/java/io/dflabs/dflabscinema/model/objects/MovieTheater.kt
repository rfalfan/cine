package io.dflabs.dflabscinema.model.objects

/**
 * Created by danielgarciaalvarado on 5/19/18.
 * DflabsCinema
 */
data class MovieTheater (
    var id: Long? = -1,
    var name: String,
    var latitude: Double,
    var longitude: Double
)