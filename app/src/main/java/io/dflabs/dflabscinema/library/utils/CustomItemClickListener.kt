package io.dflabs.dflabscinema.library.utils
import android.view.View

interface CustomItemClickListener {
    fun onItemClick(v: View, position: Int)
}