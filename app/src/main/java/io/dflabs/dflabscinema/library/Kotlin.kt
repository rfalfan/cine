package io.dflabs.dflabscinema.library

/**
 * Created by danielgarciaalvarado on 4/28/18.
 * DflabsCinema
 */


data class Movie (var name: String = "",
                 var classification: String = "",
                 var description: String = "",
                 var image: String = "",
                 var duration: Int = 0,
                 var language: String = "")



object Kotlin {

    fun execute() {
        sum2(4, null)

        var a = ""
        var a2 = ""

        val b = ""
        val b2 = ""

        if (a == "") {

        }
    }

    fun sum(a: Int, b: Int): Int = a + b

    fun sum2(a: Int, b: Int? = 0): Int {
        return a + (b ?: 0)
    }
}


class Kotlin2 {
    companion object {
        fun sum(a: Int, b: Int): Int{
            return a + b
        }
    }
}