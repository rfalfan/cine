package io.dflabs.dflabscinema.library.utils

object NotificationKeys {
    val UPDATE_MOVIES = "update_movies"
    val CUSTOM_NOTIFICATION = "custom_notification"
}
