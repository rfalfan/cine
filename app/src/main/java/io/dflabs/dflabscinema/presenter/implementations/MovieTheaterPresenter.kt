package io.dflabs.dflabscinema.presenter.implementations

import android.content.Context
import io.dflabs.dflabscinema.background.ws.WebServices
import io.dflabs.dflabscinema.library.utils.Keys
import io.dflabs.dflabscinema.library.utils.Preferences
import io.dflabs.dflabscinema.model.objects.MovieTheater
import io.dflabs.dflabscinema.presenter.callbacks.MoviesTheaterCallback
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by danielgarciaalvarado on 5/19/18.
 * DflabsCinema
 */
class MoviesTheaterPresenter(val callback: MoviesTheaterCallback) {

    var call : Call<ArrayList<MovieTheater>>? = null

    fun fetchMovieTheaters(context: Context) {
        callback.onLoadingMoviesTheaters()
        val token = Preferences.getInstance(context)?.getString(Keys.PREF_TOKEN)
        if (token == null) {
            callback.onErrorFetchingMoviesTheaters()
            return
        }
        call = WebServices.api.movieTheaters("Token ${token}")
        call?.enqueue(object : Callback<ArrayList<MovieTheater>> {
            override fun onFailure(call: Call<ArrayList<MovieTheater>>?, t: Throwable?) {
                callback.onErrorFetchingMoviesTheaters()
            }

            override fun onResponse(call: Call<ArrayList<MovieTheater>>?, response: Response<ArrayList<MovieTheater>>?) {
                if (response?.isSuccessful != null && response.isSuccessful) {
                    val movieTheaters = response.body()
                    callback.onSuccessMoviesTheaters(movieTheaters ?: ArrayList())
                } else {
                    callback.onErrorFetchingMoviesTheaters()
                }
            }
        })
    }
    fun onDestroy() {

    }
}