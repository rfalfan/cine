package io.dflabs.dflabscinema.presenter.callbacks
import io.dflabs.dflabscinema.model.objects.Movie
interface MovieCallBack {
    fun onLoadingMovie(localMovie: Movie)

    fun onSuccessMovie(movie: Movie)

    fun onErrorFetchingMovie(localMovie: Movie)
}

