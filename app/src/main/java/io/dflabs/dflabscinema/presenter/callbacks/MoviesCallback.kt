package io.dflabs.dflabscinema.presenter.callbacks

import io.dflabs.dflabscinema.model.objects.Movie

/**
 * Created by danielgarciaalvarado on 5/5/18.
 * DflabsCinema
 */
interface MoviesCallback {
    fun onLoadingMovies(localMovies: ArrayList<Movie>)

    fun onSuccessMovies(movies: ArrayList<Movie>)

    fun onErrorFetchingMovies(localMovies: ArrayList<Movie>)
}