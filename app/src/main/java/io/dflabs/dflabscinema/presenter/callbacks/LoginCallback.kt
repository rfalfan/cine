package io.dflabs.dflabscinema.presenter.callbacks

/**
 * Created by danielgarciaalvarado on 5/5/18.
 * DflabsCinema
 */
interface LoginCallback {

    fun onLoadingLogin()

    fun onSuccessLogin()

    fun onErrorLogin(error: Int)

}