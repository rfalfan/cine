package io.dflabs.dflabscinema.presenter.implementations

import android.content.Context
import io.dflabs.dflabscinema.background.ws.WebServices
import io.dflabs.dflabscinema.background.ws.requests.LoginRequest
import io.dflabs.dflabscinema.background.ws.responses.LoginResponse
import io.dflabs.dflabscinema.library.utils.Keys
import io.dflabs.dflabscinema.library.utils.Preferences
import io.dflabs.dflabscinema.presenter.callbacks.LoginCallback
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by danielgarciaalvarado on 5/5/18.
 * DflabsCinema
 */
class LoginPresenter(val callback: LoginCallback) {


    private var call: Call<LoginResponse>? = null

    fun login(username: String, password: String, context: Context){
        callback.onLoadingLogin()

        call = WebServices.accounts.login(LoginRequest(username, password))

        /*
        *  Synchronous call (Other purposes)
        val response = call.execute()
        val loginResponse = response.body()
        loginResponse?.let {
            println(it.token)
        }*/


        call?.enqueue(object : Callback<LoginResponse>{
            override fun onFailure(call: retrofit2.Call<LoginResponse>?, t: Throwable?) {
                callback.onErrorLogin(-1)
            }

            override fun onResponse(call: retrofit2.Call<LoginResponse>?, response: Response<LoginResponse>?) {
                if (response?.isSuccessful != null && response.isSuccessful){

                    val loginResponse = response.body()
                    loginResponse?.let {
                        Preferences.getInstance(context)?.putString(Keys.PREF_TOKEN, it.token)
                    }

                    callback.onSuccessLogin()
                } else {
                    callback.onErrorLogin(response?.code()!!)
                }
            }

        })
    }

    fun onDestroy() {
        call?.let {
            if (it.isExecuted){
                it.cancel()
            }
        }
    }
}