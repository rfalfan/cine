package io.dflabs.dflabscinema.presenter.implementations

import android.content.Context
import io.dflabs.dflabscinema.background.ws.WebServices
import io.dflabs.dflabscinema.library.utils.Keys
import io.dflabs.dflabscinema.library.utils.Preferences
import io.dflabs.dflabscinema.model.management.daos.MovieDatabase
import io.dflabs.dflabscinema.model.objects.Movie
import io.dflabs.dflabscinema.presenter.callbacks.MoviesCallback
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class  SingleMoviePresenter(val callback: Movie){
    
}