package io.dflabs.dflabscinema.view.fragments

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import io.dflabs.dflabscinema.R
import io.dflabs.dflabscinema.background.bus.events.UpdateMoviesEvent
import io.dflabs.dflabscinema.library.utils.Keys
import io.dflabs.dflabscinema.library.utils.Preferences
import io.dflabs.dflabscinema.model.objects.Movie
import io.dflabs.dflabscinema.presenter.callbacks.MoviesCallback
import io.dflabs.dflabscinema.presenter.implementations.MoviesPresenter
import io.dflabs.dflabscinema.view.activities.MovieTheatersActivity
import io.dflabs.dflabscinema.view.adapters.MovieAdapter
import kotlinx.android.synthetic.main.fragment_main.view.*
import kotlinx.android.synthetic.main.item_movie.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import android.R.attr.data
import android.content.ContentValues.TAG
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent


import io.dflabs.dflabscinema.library.utils.CustomItemClickListener
import kotlinx.android.synthetic.main.item_movie.view.*
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.fragment_main.*


/**
 * Created by danielgarciaalvarado on 4/28/18.
 * DflabsCinema
 */
class MainFragment : Fragment(), MoviesCallback {


    var moviesPresenter : MoviesPresenter? = null
    var adapter : MovieAdapter? = null
    var emptyView : View? = null
    var swipeRefreshLayout : SwipeRefreshLayout? = null
    var editTextSearch: EditText?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = view.fr_main_trending

        val recyclerViewNew = view.fr_main_new
        emptyView = view.fr_main_empty
        swipeRefreshLayout = view.fr_main_swipe_refresh_layout

        emptyView?.setOnClickListener {
            moviesPresenter?.fetchMovies(context!!)
        }

        swipeRefreshLayout?.setOnRefreshListener {
            moviesPresenter?.fetchMoviesRx(context!!)
        }

        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerViewNew.itemAnimator = DefaultItemAnimator()



        adapter = MovieAdapter(ArrayList(), object : CustomItemClickListener {
            override fun onItemClick(v: View, position: Int) {
                System.out.print("ssssssssssssssss");
                Log.d(TAG, "clicked position:$position")
                val postId =  v.item_movie_title
                // do what ever you want to do with it
            }
        })

        recyclerView.adapter = adapter
        recyclerViewNew.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        recyclerViewNew.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        val logoImageView = view.fr_main_image_logo as ImageView
        logoImageView.setOnClickListener {
            /*
            Preferences.getInstance(context!!)?.putString(Keys.PREF_TOKEN, null)
            activity?.finish()
            */
            startActivity(Intent(context, MovieTheatersActivity::class.java))
        }

        moviesPresenter = MoviesPresenter(this)
        moviesPresenter?.fetchMovies(context!!)
        editTextSearch = fr_main_search
        editTextSearch?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(editable: Editable) {
                //after the change calling the method and passing the search input
                filter(editable.toString())
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })


    }

    fun filter( texto: String ){
        val filterdNames = ArrayList<Movie>()
        if(!texto.trim().equals("")){
            for (s in adapter!!.movies) {
                //if the existing elements contains the search input
                if ( s.title.toLowerCase().contains(texto.toLowerCase())) {
                    //adding the element to filtered list
                    filterdNames.add(s)
                }

            }
            if(filterdNames.isEmpty()){
                moviesPresenter?.fetchMovies(context!!)
                filterdNames.addAll(adapter!!.movies)
            }
            adapter?.filterList(filterdNames)
        }else{
            moviesPresenter?.fetchMovies(context!!)
            filterdNames.addAll(adapter!!.movies)
        }


    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUpdateMoviesEvent(event: UpdateMoviesEvent){
        moviesPresenter?.fetchMovies(context!!)
    }

    override fun onDestroy() {
        super.onDestroy()
        moviesPresenter?.onDestroy()
    }

    override fun onLoadingMovies(localMovies: ArrayList<Movie>) {
        adapter?.update(localMovies)
    }

    override fun onSuccessMovies(movies: ArrayList<Movie>) {
        updateView(movies)
    }

    override fun onErrorFetchingMovies(localMovies: ArrayList<Movie>) {
        updateView(localMovies)
        val snackBar = Snackbar.make(view!!, R.string.dialog_error_loading_movies, Snackbar.LENGTH_INDEFINITE)
        snackBar.setAction(R.string.action_retry) {
            snackBar.dismiss()
            moviesPresenter?.fetchMovies(context!!)
        }

        snackBar.view.setBackgroundColor(ContextCompat.getColor(context!!, android.R.color.white))
        snackBar.show()
    }

    fun updateView(movies: ArrayList<Movie>) {
        swipeRefreshLayout?.isRefreshing = false
        if (movies.size > 0) {
            swipeRefreshLayout?.visibility = View.VISIBLE
            adapter?.update(movies)
        } else {
            swipeRefreshLayout?.visibility = View.GONE
            emptyView?.visibility = View.VISIBLE
        }
    }
}