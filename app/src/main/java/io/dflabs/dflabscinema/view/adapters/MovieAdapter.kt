package io.dflabs.dflabscinema.view.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import io.dflabs.dflabscinema.R
import io.dflabs.dflabscinema.library.utils.download
import io.dflabs.dflabscinema.model.objects.Movie
import  io.dflabs.dflabscinema.library.utils.RecyclerItemClickListener
import android.widget.Toast
import kotlinx.android.synthetic.main.item_movie.view.*
import android.support.v7.widget.RecyclerView.ViewHolder
import io.dflabs.dflabscinema.library.utils.CustomItemClickListener
import android.R.attr.data
import android.content.Context





/**
 * Created by danielgarciaalvarado on 4/28/18.
 * DflabsCinema
 */


class MovieAdapter(var movies: ArrayList<Movie> , clickListener: CustomItemClickListener) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {
    var listener: CustomItemClickListener? = clickListener
    var data = movies


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        var view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movie, parent, false)
       val mViewHolder: MovieViewHolder = MovieViewHolder(view)
     /*  mViewHolder.setOnClickListener {View.OnClickListener {
          fun onClick(v: MovieViewHolder) {
               listener?.onItemClick(v)
           }
       }

       }*/
        return  mViewHolder
    }



    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val movie = movies[position]
        holder.imageView?.download(movie.image)
        holder.titleTextView?.text = movie.title
        holder.classificationTextView?.text = movie.classification ?: ""


    }



    fun update(movies: ArrayList<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    fun filterList(filterdNames: ArrayList<Movie>) {
        this.movies = filterdNames;
        notifyDataSetChanged();
    }

    class MovieViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        val imageView = itemView?.findViewById<ImageView>(R.id.item_movie_image)
        val titleTextView = itemView?.findViewById<TextView>(R.id.item_movie_title)
        val classificationTextView = itemView?.findViewById<TextView>(R.id.item_movie_classification)
        val vista:View? = itemView

    }
}