package io.dflabs.dflabscinema.background.services

import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationCompat.DEFAULT_SOUND
import android.support.v4.app.NotificationCompat.DEFAULT_VIBRATE
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.dflabs.dflabscinema.R
import io.dflabs.dflabscinema.background.bus.events.UpdateMoviesEvent
import io.dflabs.dflabscinema.library.utils.NotificationKeys
import org.greenrobot.eventbus.EventBus

/**
 * Created by danielgarciaalvarado on 5/19/18.
 * DflabsCinema
 */
class MessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        /*
              Uncomment to let Firebase handle automatically the notification
         */
        val data = remoteMessage?.data
        if (data != null && data.size > 0){
            val customAction = data["custom_action"] as String

            when (customAction) {
                NotificationKeys.UPDATE_MOVIES -> EventBus.getDefault().post(UpdateMoviesEvent())
                NotificationKeys.CUSTOM_NOTIFICATION -> buildNotification(data)
            }
        } else {
            super.onMessageReceived(remoteMessage)
        }
    }

    private fun buildNotification(data: Map<String, String>) {
        val builder = NotificationCompat.Builder(applicationContext)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(data["custom_title"])
                .setLights(Color.CYAN, 3000, 3000)
                .setDefaults(DEFAULT_SOUND or DEFAULT_VIBRATE)
                .setAutoCancel(true)
                .setOngoing(true)
                .setContentText(data["custom_content"])
        val notification = builder.build()


        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(1, notification)
    }
}
