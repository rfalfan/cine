package io.dflabs.dflabscinema.background.ws.requests

class TokenRequest (val token: String)
