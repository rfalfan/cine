package io.dflabs.dflabscinema.background.ws.responses

data class LoginResponse(val token: String)
