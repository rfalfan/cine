package io.dflabs.dflabscinema.background.services

import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import io.dflabs.dflabscinema.background.ws.WebServices
import io.dflabs.dflabscinema.background.ws.requests.TokenRequest
import io.reactivex.schedulers.Schedulers

/**
 * Created by danielgarciaalvarado on 5/19/18.
 * DflabsCinema
 */
class InstanceIdService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        super.onTokenRefresh()
        val token = FirebaseInstanceId.getInstance().token

        /*

        RECOMMENDED: Save token into preferences and then send it to custom server in the login screen
         */
        token?.let {
            WebServices.apiRx
                    .sendToken(TokenRequest(token), "Token <INSERT TOKEN>")
                    .observeOn(Schedulers.io())
                    .subscribe({
                        print("Token successfully sent")
                    }, {
                        print("Token error send")
                    })
        }

        // TODO Send this token to my custom backend for targeted notification purposes
    }
}